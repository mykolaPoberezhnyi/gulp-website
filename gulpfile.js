var gulp = require('gulp'), // Подключаем Gulp
    scss = require('gulp-sass'), //Подключаем Sass пакет,
    browserSync = require('browser-sync').create(), // Подключаем Browser Sync
    concat = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации файлов)
    uglify = require('gulp-uglifyjs'), // Подключаем gulp-uglifyjs (для сжатия JS)
    cssnano = require('gulp-cssnano'), // Подключаем пакет для минификации CSS
    rename = require('gulp-rename'), // Подключаем библиотеку для переименования файлов
    del = require('del'), // Подключаем библиотеку для удаления файлов и папок
    imagemin = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
    pngquant = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png
    cache = require('gulp-cache'), // Подключаем библиотеку кеширования
    autoprefixer = require('gulp-autoprefixer'), // Подключаем библиотеку для автоматического добавления префиксов
    include = require('gulp-html-tag-include'), //Добавляем тег include в html
    buffer = require('vinyl-buffer'),
    csso = require('gulp-csso'),
    merge = require('merge-stream'),
    spritesmith = require('gulp.spritesmith'),
    svgSprite = require('gulp-svg-sprite');

var app = './app';
var dist = './dist';

gulp.task('scss', function() { 
    return gulp.src(app + '/scss/**/*') 
        .pipe(scss()) 
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true })) 
        // .pipe(cssnano()) Минификация
        .pipe(concat('main.css'))
        .pipe(gulp.dest(dist + '/css')) 
        .pipe(browserSync.reload({ stream: true }));  
});

gulp.task('svg-sprite', function() {
    config = {
        mode: {
            css: {		
                render: {
                    css: true
                }
            }
        }
    };
    gulp.src('sprites/**/*.svg', {cwd: app + '/scss'})
    .pipe(svgSprite(config))
    .pipe(gulp.dest(dist + '/img/'));
});

gulp.task('sprite', function () {
    var spriteData = gulp.src(app + '/sprites/**/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.scss'
    }));
 
  var imgStream = spriteData.img
    .pipe(buffer())
    .pipe(imagemin())
    .pipe(gulp.dest(dist + '/img'));
 
  var cssStream = spriteData.css
    .pipe(csso())
    .pipe(gulp.dest(app + '/scss'));
 
  return merge(imgStream, cssStream);
});

gulp.task('scripts', function() {
    return gulp.src(app + '/js/**/*')
        .pipe(concat('main.js'))
        .pipe(gulp.dest(dist + '/js'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('browser-sync', function() { 
    browserSync.init({ 
        server: { 
            baseDir: dist 
        },
        notify: false
    });
    gulp.watch(app + '/sprite', ['sprite', 'scss']); 
    gulp.watch(app + '/scss/**/*.scss', ['scss']);  
    gulp.watch(app + '/**/*.html', ['html-include']); 
    gulp.watch(app + '/js/**/*.js', ['scripts']); 
    gulp.watch(app + '/img', ['img']); 
    gulp.watch(app + '/fonts', ['fonts']); 
});

gulp.task('html-include', function() {
    return gulp.src(app + '/*.html')
        .pipe(include())
        .pipe(gulp.dest(dist))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('fonts', function() {
    return gulp.src(app + '/fonts/**/*') 
        .pipe(gulp.dest(dist + '/fonts'))
        .pipe(browserSync.reload({ stream: true }));        
});

gulp.task('clean', function() {
    return del.sync(dist); 
});

gulp.task('img', function() {
    return gulp.src(app + '/img/**/*') 
        .pipe(cache(imagemin({ 
            interlaced: true,
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            use: [pngquant()]
        })))
        .pipe(gulp.dest(dist + '/img')) 
        .pipe(browserSync.reload({ stream: true }));        
});

gulp.task('clear', function(callback) {
    return cache.clearAll();
})

gulp.task('watch', ['clear','clean', 'browser-sync', 'sprite', 'scss', 'fonts', 'html-include', 'scripts', 'img'], function() {
    
});

gulp.task('default', ['browser-sync', 'watch']);